# K-means clustering on Iris dataset

Seeing how the K-means algorithm works on the Iris dataset. 
I coded the K-means algorithm myself for this project. Even though a library could have been much easier and much more efficient, I wanted to truly understand its inner workings.

![Fisher iris categories](https://miro.medium.com/max/1400/0*GVjzZeYrir0R_6-X.png)

# The Iris dataset

The Iris dataset is a very well-known dataset that has been used since Ronald Fisher presented it in 1936. It contains the measurements for the height and the width of the petals and the sepals on 150 iris samples across three diffreent varieties: Iris setosa, Iris versicolor, and Iris virginica.
By comparating the differences in petal sizes and sepal sizes, we can find the three varieties of irises from scratch.

# Author
Justine Cassiopée Verdeyrout, even though of course this is a basic clustering exercise and a million versions of it probablty exist.
